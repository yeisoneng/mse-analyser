from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'msecloud.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^', include("core.urls")),

    url(r"^about/$", TemplateView.as_view(template_name="about.html"), name="about"),

    url(r'^admin/', include(admin.site.urls)),
]
