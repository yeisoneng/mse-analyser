from django.views.generic.base import View
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils.decorators import method_decorator

import random
import os

from numpy import arange, sinc, linspace, pi, sin, frombuffer, linspace
from scipy.io.wavfile import read

from django.core.files.storage import default_storage
from django.core.files.base import ContentFile


THINKS_THEMES = {

    "Verónica": ['#ccff42', '#74e400', '#99cc33', '#8dd35f', '#5aa02c', '#93ac93', '#536c53', '#338000'],
    "Nathalia": ['#bae4e5', '#1ca8dd', '#1693a5', '#00ccff', '#6495ed', '#2a8fbd', '#61d2d6', '#025d8c'],
    "Daniela":  ['#7f2aff', '#9955ff', '#ab37c8', '#d42aff', '#c6afe9', '#e580ff', '#cd87de', '#a793ac'],
    # "Ángela":   ['#00112b', '#214478', '#373e48', '#2d1650', '#53536c', '#2d5016', '#554400', '#501616'],
    "Nessa":    ['#ffd5d5', '#ffccaa', '#ffe680', '#eeffaa', '#aaffee', '#d7eef4', '#e9afdd', '#ffaacc'],
    "Ángela":   ['#cc0000', '#ff9900', '#ffcc00', '#669900', '#99cc33', '#003399', '#3366cc', '#6699ff'],

}


MSE_BIN = os.path.join(os.path.dirname(__file__), "mse", "mse")
DATA_DIR = os.getenv("OPENSHIFT_DATA_DIR", os.path.join(os.path.dirname(__file__), "DATA_DIR"))


########################################################################
class MSE(View):
    """"""
    template = "mse.html"

    #----------------------------------------------------------------------
    def get(self, request, *args, **kwargs):
        """"""
        view_theme = random.choice(THINKS_THEMES["Ángela"])

        # X_data = linspace(0, 2*pi, 40)
        # Y_data = list(abs(1+sin(X_data)))
        # X_data = list(range(1, 41))
        X_data = []
        Y_data = []
        return render(request, self.template, locals())


########################################################################
class Plot(View):
    """"""
    template = "widgets/uploader.html"

    #----------------------------------------------------------------------
    def get(self, request, *args, **kwargs):
        """"""
        view_theme = random.choice(THINKS_THEMES["Ángela"])

        form_id = ''.join([random.choice('abcdefghijklmnopqrstuvwxyz0123456789') for n in range(10)])
        # X_data = linspace(0, 2*pi, 40)
        # Y_data = list(abs(1+sin(X_data)))
        # X_data = list(range(1, 41))
        X_data = []
        Y_data = []

        return render(request, self.template, locals())



    #----------------------------------------------------------------------
    # @method_decorator(ensure_csrf_cookie)
    def post(self, request, *args, **kwargs):
        """"""
        data_js = {}

        a = int(request.POST["a"])
        n = int(request.POST["n"])
        r = float(request.POST["r"])
        file = request.FILES["file"]

        data = self.get_data(file)

        mse, mse_content = self.calculate_mse(data, n, a, r)

        if mse_content is False:
            return JsonResponse({"status": "Failed",})

        data_mse = []
        data_n = []
        for n, d in mse:
            data_mse.append(d)
            data_n.append(n)

        data_js["data"] = data_mse
        data_js["n"] = data_n
        data_js["csv_content"] = "\n".join([str(float(d)) for d in data])
        data_js["mse_content"] = mse_content

        return JsonResponse(data_js)



    #----------------------------------------------------------------------
    def get_data(self, file):
        """"""

        if file.name.endswith(".wav"):
            name = ''.join([random.choice('abcdefghijklmnopqrstuvwxyz0123456789') for n in range(10)])
            wav_filename = os.path.join(DATA_DIR, name+".wav")
            default_storage.save(wav_filename, ContentFile(file.read()))
            rate, data = read(wav_filename)
            time = linspace(0, len(data)/rate, len(data))
            os.remove(wav_filename)
            return list(data)

        elif file.name.endswith(".txt") or file.name.endswith(".csv"):
            return [float(f) for f in filter(None, lines)]





    #----------------------------------------------------------------------
    def save_file(self, data):
        """"""
        name = ''.join([random.choice('abcdefghijklmnopqrstuvwxyz0123456789') for n in range(10)])
        filename = os.path.join(DATA_DIR, name+".data")

        file = open(filename, "w")
        if data:
            for d in data:
                file.write(str(d) + "\n")
            file.close()
            return filename
        else:
            return False




    #----------------------------------------------------------------------
    def calculate_mse(self, data, n, a, r):
        """"""
        filename = self.save_file(data)

        if not filename:
            return False, False

        filename_out = filename.replace(".data", ".mse")
        os.system("{} -n {} -a {} -r {} <'{}' >'{}'".format(MSE_BIN, n, a, r, filename, filename_out))

        file = open(filename_out, "r")
        lines = file.readlines()
        file.close()

        mse = list(map(lambda x:(int(x[:x.find("\t")].replace("\n", "").replace("\t", "")), abs(float(x[x.find("\t")+1:].replace("\n", "").replace("\t", "")))), lines[4:]))

        os.remove(filename)
        os.remove(filename_out)

        return mse, "".join(lines)