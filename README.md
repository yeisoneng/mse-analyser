# Django template #

This is a simple Django template for personal use, but shared with everyone!!

## Include ##
 *  Bootsrap 3.3.5 and 4.0.0-alpha
 *  JQuery 2.1.4
 *  JQuery Plugins
  *  browser
  *  cookie
  *  custom-protocol
  *  easing
  *  form
  *  jscroll
*  Thether 1.1.0
*  Font Awesome
*  Brython
*  Chart.js 1.0.2 and 2.0.0-aplha3
*  Ace editor 1.2.0
*  and more...
