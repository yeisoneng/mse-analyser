for align, text in zip('<^>', ['left', 'center', 'right']):
  print('{0:{fill}{align}16}'.format(text, fill=align, align=align))

print('{0:>>16}'.format('left'))