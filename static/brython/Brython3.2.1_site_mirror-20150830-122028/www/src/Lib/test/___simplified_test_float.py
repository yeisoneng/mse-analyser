import sys

class CtxManager:
    
    def __init__(self, exception, **kw):
        self.exception = exception
        self.kw = kw
    
    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, *args):
        if exc_type is None or not issubclass(exc_type, self.exception):
            raise AssertionError('should have raised %s but raised %s'
                %self.exception, exc_type)
        return True

class TestCase:
    
    def assertEqual(self, result, expected):
        if result != expected:
            raise AssertionError('expected %s, got %s' %(expected, result))

    def assertIsInstance(self, obj, klass):
        if not isinstance(obj, klass):
            print(obj,'is not instance of', klass)

    def assertRaises(self, exception, *args, **kw):
        if args:
            callable, *args = args
            try:
                callable(*args, **kw)
            except Exception as exc:
                if not isinstance(exc, exception):
                    print(callable, args, kw, 'does not raise', exception)
        else:
            return CtxManager(exception, **kw)

    def assertTrue(self, expr):
        if not expr:
            raise AssertionError('expr not True : %s' %expr)

INF = float("inf")
NAN = float("nan")

class FormatTestCase(TestCase):

    def test_format(self):
        # these should be rewritten to use both format(x, spec) and
        # x.__format__(spec)

        self.assertEqual(format(0.0, 'f'), '0.000000')

        # the default is 'g', except for empty format spec
        self.assertEqual(format(0.0, ''), '0.0')
        self.assertEqual(format(0.01, ''), '0.01')
        self.assertEqual(format(0.01, 'g'), '0.01')

        # empty presentation type should format in the same way as str
        # (issue 5920)
        x = 100/7.
        self.assertEqual(format(x, ''), str(x))
        self.assertEqual(format(x, '-'), str(x))
        self.assertEqual(format(x, '>'), str(x))
        self.assertEqual(format(x, '2'), str(x))

        self.assertEqual(format(1.0, 'f'), '1.000000')

        self.assertEqual(format(-1.0, 'f'), '-1.000000')

        self.assertEqual(format( 1.0, ' f'), ' 1.000000')
        self.assertEqual(format(-1.0, ' f'), '-1.000000')
        self.assertEqual(format( 1.0, '+f'), '+1.000000')
        self.assertEqual(format(-1.0, '+f'), '-1.000000')

        # % formatting
        self.assertEqual(format(-1.0, '%'), '-100.000000%')

        # conversion to string should fail
        self.assertRaises(ValueError, format, 3.0, "s")

        # other format specifiers shouldn't work on floats,
        #  in particular int specifiers
        for format_spec in ([chr(x) for x in range(ord('a'), ord('z')+1)] +
                            [chr(x) for x in range(ord('A'), ord('Z')+1)]):
            if not format_spec in 'eEfFgGn%':
                self.assertRaises(ValueError, format, 0.0, format_spec)
                self.assertRaises(ValueError, format, 1.0, format_spec)
                self.assertRaises(ValueError, format, -1.0, format_spec)
                self.assertRaises(ValueError, format, 1e100, format_spec)
                self.assertRaises(ValueError, format, -1e100, format_spec)
                self.assertRaises(ValueError, format, 1e-100, format_spec)
                self.assertRaises(ValueError, format, -1e-100, format_spec)

        # issue 3382
        self.assertEqual(format(NAN, 'f'), 'nan')
        self.assertEqual(format(NAN, 'F'), 'NAN')
        self.assertEqual(format(INF, 'f'), 'inf')
        self.assertEqual(format(INF, 'F'), 'INF')

    def test_issue5864(self):
        self.assertEqual(format(123.456, '.4'), '123.5')
        self.assertEqual(format(1234.56, '.4'), '1.235e+03')
        self.assertEqual(format(12345.6, '.4'), '1.235e+04')


def run(test_class):
    tester = test_class()
    for attr in dir(tester):
        if attr.startswith('test_'):
            print(attr)
            getattr(tester, attr)()

run(FormatTestCase)

