class CtxManager:
    
    def __init__(self, exception, **kw):
        self.exception = exception
        self.kw = kw
    
    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, *args):
        if exc_type is None or not issubclass(exc_type, self.exception):
            raise AssertionError('should have raised %s but raised %s'
                %self.exception, exc_type)
        return True

class TestCase:
    
    def assertEqual(self, result, expected):
        if result != expected:
            raise AssertionError('expected %s, got %s' %(expected, result))

    def assertIsInstance(self, obj, klass):
        if not isinstance(obj, klass):
            print(obj,'is not instance of', klass)

    def assertRaises(self, exception, *args, **kw):
        if args:
            callable, *args = args
            try:
                callable(*args, **kw)
            except Exception as exc:
                if not isinstance(exc, exception):
                    print(callable, args, kw, 'does not raise', exception)
        else:
            return CtxManager(exception, **kw)

    def assertTrue(self, expr):
        if not expr:
            raise AssertionError('expr not True : %s' %expr)


def skipUnless(condition, reason):
    if condition:
        return lambda f:f
    else:
        return lambda f:True

def skip(reason):
    return lambda f:True