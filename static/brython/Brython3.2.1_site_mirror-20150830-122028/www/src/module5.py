def P():
    b=1
    def Q():
        nonlocal b
        b+=1
        return b
    return Q()
assert P()==2