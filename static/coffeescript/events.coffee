
$(document).ready ->



#csrftoken for AJAX
#----------------------------------------------------------------------
    #Ajax setup for use CSRF tokens
    csrftoken = $.cookie("csrftoken")
    csrfSafeMethod = (method) -> (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method))
    $.ajaxSetup
        beforeSend: (xhr, settings) ->
            if not csrfSafeMethod(settings.type) and not @crossDomain
                xhr.setRequestHeader("X-CSRFToken", csrftoken)
    #PinguinoCloud.csrftoken = csrftoken
#----------------------------------------------------------------------


    $(document).on "change", "input[type=file]", (event) ->
    #$("input[type=file]").on "change", (event) ->
        $("#"+$(@).attr("id")+"_fake").val($(@).val())



    $(document).on "click", "button.btn-submit", (event) ->
    #$("button.btn-submit").on "click", (event) ->
        event.preventDefault()
        form_id = $(@).data("form_id")
        $(".fa-working-spin-#{form_id}").fadeIn()

        $("form##{form_id}").ajaxSubmit
            success: (returnData) ->

                if returnData.status is "Failed"
                    $(".fa-working-spin-#{form_id}").fadeOut()
                    alert("Failed!!")
                    return

                n = returnData.n
                data = returnData.data

                line = eval("window.line_#{form_id}")
                line.clear()

                for i in line.datasets[0].points
                    line.removeData()

                j=0
                for i in n
                    line.addData([0], i)
                    line.datasets[0].points[j].value = data[j]
                    j++
                $(".fa-working-spin-#{form_id}").fadeOut()
                alert("Completed!!")
                line.update();

                csv_content = encodeURI(returnData.csv_content)
                mse_content = encodeURI(returnData.mse_content)


                $(".btn-csv-#{form_id}").attr("href","data:text/plain;charset=utf-8,#{csv_content}")
                $(".btn-mse-#{form_id}").attr("href","data:text/plain;charset=utf-8,#{mse_content}")

                filename = $("##{form_id}_id_file_fake").val()

                $(".btn-csv-#{form_id}").attr("download", filename+".csv")
                $(".btn-mse-#{form_id}").attr("download", filename+"-mse.txt")



        $(".fa-working-spin").fadeOut()




    $(".btn-add-analyser").on "click", (event) ->
        $.get "/plot/", (html) ->
            $(".placeholder-analyser").append(html)
